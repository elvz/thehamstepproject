const btnPrev = document.querySelector(".arrow-left");
const btnNext = document.querySelector(".arrow-right");
const slides = document.querySelector(".slider").children;
const smallImgs = document.querySelector(".dots").children;

for (let i = 0; i < smallImgs.length; i++) {
  smallImgs[i].addEventListener("click", (e) => {
    for (let j = 0; j < smallImgs.length; j++) {
      smallImgs[j].classList.remove("small-img-active");
    }
    e.target.classList.add("small-img-active");

    for (let j = 0; j < slides.length; j++) {
      slides[j].classList.replace("feedback-item", "d-none");
    }

    slides[i].classList.replace("d-none", "feedback-item");
  });
}
